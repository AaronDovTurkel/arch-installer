#!/usr/bin/env bash


echo -e "\nINSTALLING PACMAN PKGS\n"

PACMAN_PKGS=(
    'cronie'                    # cron jobs
    'gtop'                      # System monitoring via terminal
    'gufw'                      # Firewall manager
    'hardinfo'                  # Hardware info app
    'numlockx'                  # Turns on numlock in X11
    'terminus-font'             # Font package with some bigger fonts for login terminal
    'zip'                       # Zip compression program
    'tmux'                      # Terminal multiplexer
    'zenity'                    # Display graphical dialog boxes via shell 
    'lightdm-webkit2-greeter'   # Framework for Awesome Login Themes
    'lightdm-gtk-greeter-settings'
    'flameshot'                 # Screenshots
    'nautilus'                  # Filesystem browser
    'veracrypt'                 # Disc encryption utility
    'meld'                      # File/directory comparison
    'gcolor2'                   # Colorpicker
    'xpdf'                      # PDF viewer
    'volumeicon'                # System tray volume control
    'pulseaudio-bluetooth'      # Bluetooth support for PulseAudio
    'ghostscript'               # PostScript interpreter
    'gsfonts'                   # Adobe Postscript replacement fonts
    'hplip'                     # HP Drivers
    'system-config-printer'     # Printer setup  utility
    'feh'                       # Lightweight and powerful image viewer 
    'qt5ct'                     # Qt set theme icon eg.
    'avahi'                     # Netwrok discovery
)

for PKG in "${PACMAN_PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    sudo pacman -S "$PKG" --noconfirm --needed
done

echo -e "\nDone!\n"

echo -e "\nINSTALLING AUR PACKAGES\n"

AUR_PKGS=(
    'pamac-aur'                 # AUR package manager
    
)

for PKG in "${AUR_PKGS[@]}"; do
    echo "INSTALLING: ${PKG}"
    yay -S "$PKG" --noconfirm --needed
done

echo -e "\nDone!\n"


